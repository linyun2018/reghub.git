import Vue from 'vue'
import App from './App.vue'
import VueClipboard from 'vue-clipboard2'
// 按需引入element-ui
import 'element-ui/lib/theme-chalk/index.css'
import {
  Alert,
  Button as EButton,
  Container,
  Header,
  Input,
  Main,
  Message,
  Table,
  TableColumn,
  Tag,
  Tooltip
} from 'element-ui'
// 剪贴板
Vue.use(VueClipboard)

Vue.config.productionTip = false
/* 组件有 2 种引入组件方式( component 或者 use ),当出现和别的组件重名的时候,用 as 别名. */
Vue.use(EButton)
Vue.use(Container)
Vue.use(Main)
Vue.use(Header)
Vue.use(Table)
Vue.use(TableColumn)
Vue.use(Tag)
Vue.use(Alert)
Vue.use(Tooltip)
Vue.use(Input)
Vue.prototype.$message = Message

new Vue({
  render: h => h(App)
}).$mount('#app')
