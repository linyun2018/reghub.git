### 常用正则表达式

目前只有部分正则表达式录入，并且默认是java语言的，其实js也可以用。
如果选择复制demo，请注意`反斜杠`的问题。

后继会陆续加入更多的正则表达式，当然我比较懒更新的慢，大家把开发中常用的正则发给我，:)。

#### 联系方式
邮箱：664162337@qq.com

微博：[@林建国是只猫](https://weibo.com/u/6213567477)

#### 开源
本项目开源地址：
[https://gitee.com/linyun2018/reghub.git](https://gitee.com/linyun2018/reghub.git)

#### Q&A

##### 1. 编译后浏览首页打开的是空白页面
检查 vue.config.js 配置文件中 `publicPath` 是否配置正确，`publicPath：'./'`

##### 2. v-html 渲染的 html 希望可以监控点击事件
在 v-html 同级加绑定事件，
```html
<code v-html="row.demo" @click="clickEvent($event)"/>
```
methods：
```js
clickEvent(e) {
    // 默认在v-html渲染的代码中不能使用vue的方法，但是可以通过父级的监控去获取用户操作事件
    if (e.target.nodeName.toLowerCase() === 'a') {
      // 调用utools的方法进行跳转
      utools.ubrowser.goto(e.target.href)
      .run()
    }
}
```
